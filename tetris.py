from __future__ import print_function

import threading
import time
import sys
from random import choice

from shapes import colors, shapes


class Shape(object):
    rotate = 0
    current_shape = None
    current_rotate = None
    speed = 1

    @staticmethod
    def get_shape():
        Shape.current_shape = Shape.current_shape or shapes[choice(shapes.keys())]
        Shape.rotate = int(choice(Shape.current_shape.keys()))
        Shape.current_rotate = Shape.current_rotate or \
            Shape.current_shape[str(Shape.rotate)]
        return Shape.current_rotate

    @staticmethod
    def change_rotate():
        while True:
            raw_input()
            if not Shape.current_shape:
                continue

            try:
                Shape.rotate = Shape.rotate + 1
                Shape.current_rotate = Shape.current_shape[str(Shape.rotate)]
            except KeyError:
                Shape.rotate = 0
                Shape.current_rotate = Shape.current_shape[str(Shape.rotate)]


class Tetris(object):
    placeholder = '* '
    CURSOR_UP_ONE = '\x1b[1A'
    ERASE_LINE = '\x1b[2K'

    def __init__(self, row=40, col=40):
        self.row, self.col = row, col
        self.board_matrix = {(i, j): 0 for i in range(self.row + 1) for j in range(self.col + 1)}
        self.current_shape = None
        self.ended_shapes = []

    def clear_board(self):
        self.board_matrix = {(i, j): 0 for i in range(self.col + 1) for j in range(self.row + 1)}

    def clear_console(self):
        for i in range(0, self.row + 50):
            sys.stdout.write(self.CURSOR_UP_ONE)

    def create_board(self):
        row_print = ''
        for row in range(self.row + 1):
            for col in range(self.col + 1):
                if row == self.row or col == 0:
                    row_print += self.placeholder
                else:
                    if col == self.col:
                        row_print += self.placeholder
                    else:
                        row_print += '{}{} '.format(colors[str(self.board_matrix[col, row])], ' ')
            row_print += '\r\n'
        return row_print

    def run(self):
        row_count = 0
        while True:
            self.clear_console()
            self.clear_board()
            shape = Shape.get_shape()

            for i, row in enumerate(shape):
                for k, col in enumerate(row):
                    self.board_matrix[i + 10, k + row_count] = col
                    if row_count + 1 + k == self.row:
                        row_count = 0
                        Shape.current_shape, Shape.current_rotate = None, None
                        Shape.speed = 1
                        break

            print(self.create_board())
            time.sleep(Shape.speed)
            row_count += 1


tetris = Tetris(col=30, row=30)
t_run = threading.Thread(target=tetris.run)
t_rotate = threading.Thread(target=Shape.change_rotate)

t_rotate.start()
t_run.start()
